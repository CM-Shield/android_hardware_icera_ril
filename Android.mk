ifeq ($(TARGET_TEGRA_MODEM),icera)

LOCAL_PATH:= $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := \
    atchannel.c \
    at_channel_handle.c \
    at_channel_writer.c \
    at_channel_config.c \
    ril_request_queue.c \
    ril_request_object.c \
    ril_unsol_object.c \
    misc.c \
    at_tok.c \
    icera-ril-net.c \
    icera-ril-data.c \
    icera-ril-call.c \
    icera-ril-sms.c \
    icera-ril-sim.c \
    icera-ril-stk.c \
    icera-ril-services.c \
    icera-ril-utils.c \
    icera-ril-logging.c \
    icera-ril-fwupdate.c \
    icera-ril-power.c \
    icera-ril.c

LOCAL_SHARED_LIBRARIES := \
    libcutils libutils libril libnetutils

LOCAL_STATIC_LIBRARIES := \
    libutil-icera

# for asprinf
LOCAL_CFLAGS := -D_GNU_SOURCE

LOCAL_C_INCLUDES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr/include
LOCAL_ADDITIONAL_DEPENDENCIES += $(TARGET_OUT_INTERMEDIATES)/KERNEL_OBJ/usr

LOCAL_C_INCLUDES += \
    $(LOCAL_PATH)/icera-util \
    external/boringssl/include \
    bionic/libc/upstream-netbsd/android/include

ifeq ($(TARGET_BUILD_TYPE),debug)
    LOCAL_CFLAGS += -DESCAPE_IN_ECCLIST
endif

LOCAL_CFLAGS += -DRIL_SHLIB
LOCAL_MODULE:= libril-icera-core

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

# Build static library
LOCAL_MODULE:= libutil-icera

LOCAL_SRC_FILES:= \
    icera-util/icera-util-power.c \
    icera-util/icera-util-wakelock.c \
    icera-util/icera-util-crashlogs.c

LOCAL_SHARED_LIBRARIES := \
    libcutils

include $(BUILD_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

LOCAL_SRC_FILES := icera-ril-extension-stub.c

LOCAL_WHOLE_STATIC_LIBRARIES := \
    libutil-icera libril-icera-core

    #build shared library
    LOCAL_SHARED_LIBRARIES += \
        libcutils libutils libnetutils libril libcrypto librilutils

    LOCAL_MULTILIB := first
    LOCAL_MODULE := libril-icera

    include $(BUILD_SHARED_LIBRARY)

endif
