LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE_TAGS := optional

# Build static library
LOCAL_MODULE:= libutil-icera

LOCAL_SRC_FILES:= \
    icera-util-power.c \
    icera-util-wakelock.c \
    icera-util-crashlogs.c

LOCAL_SHARED_LIBRARIES := \
    libcutils

include $(BUILD_STATIC_LIBRARY)

